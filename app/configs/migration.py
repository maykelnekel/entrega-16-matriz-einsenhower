from flask import Flask
from flask_migrate import Migrate

def init_app(app: Flask):
    
    # IMPORTE AQUI SUAS CLASSES
    from app.models.categories_model import CategoriesModel
    from app.models.einsenhowers_model import EinsenhowersModel
    from app.models.tasks_catergories_model import tasks_categories
    from app.models.tasks_model import TasksModel

    Migrate(app, app.db)
