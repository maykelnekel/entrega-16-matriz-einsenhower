from flask import Blueprint

# IMPORTE SEUS CONTROLLERS
from app.controllers.category_controller import create_one, delete_one, get_all, update_one

# REGISTRE SUA BLUEPRINT
bp = Blueprint('category_bp', __name__, url_prefix='/category')


# INICIE SUAS ROTAS
bp.post("")(create_one)
bp.get("")(get_all)
bp.patch("/<int:id>")(update_one)
bp.delete("/<int:id>")(delete_one)
