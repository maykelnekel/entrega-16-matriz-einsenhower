from flask import Blueprint

from app.controllers.task_controller import create_one, delete_one, get_all, update_one

# REGISTRE SUA BLUEPRINT
bp = Blueprint('task_bp', __name__, url_prefix='/')


# INICIE SUAS ROTAS
bp.post("task")(create_one)
bp.get("")(get_all)
bp.patch("task/<int:id>")(update_one)
bp.delete("task/<int:id>")(delete_one)
