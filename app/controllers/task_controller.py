import pdb
from flask import jsonify, request, current_app
from sqlalchemy.orm import column_property
from app.models.categories_model import CategoriesModel
from app.models.tasks_catergories_model import tasks_categories
from app.models.tasks_model import TasksModel
from app.models.einsenhowers_model import EinsenhowersModel
import sqlalchemy


def found_eisenhower(importance, urgency):
    eisenhower_classification = ""

    if importance == 1 and urgency == 1:
        eisenhower_classification = "Do It First"
    elif importance == 1 and urgency == 2:
        eisenhower_classification = "Schedule It"
    elif importance == 2 and urgency == 1:
        eisenhower_classification = "Delegate It"
    elif importance == 2 and urgency == 2:
        eisenhower_classification = "Delete It"

    return eisenhower_classification


def create_one():
    try:
        data = request.get_json()
        importance = data['importance']
        urgency = data['urgency']
        categories = data.pop('categories')

        if importance < 0 or importance > 2 or urgency < 0 or urgency > 2:
            return {"error": {
                "valid_options": {
                    "importance": [
                        1,
                        2
                    ],
                    "urgency": [
                        1,
                        2
                    ]
                },
                "recieved_options": {
                    "importance": importance,
                    "urgency": urgency
                }
            }}, 404

        eisenhower = EinsenhowersModel.query.filter_by(
            type=found_eisenhower(importance, urgency)).first()
        for item in categories:

            category = CategoriesModel.query.filter_by(
                name=item['name']).first()

            if category == None:
                new_category_data = {
                    "name": item["name"],
                    "description": ""
                }
                new_category = CategoriesModel(**new_category_data)
                current_app.db.session.add(new_category)
                current_app.db.session.commit()

        task = TasksModel(**data)
        task.einsenhower_id = eisenhower.id
        for item in categories:
            category = CategoriesModel.query.filter_by(
                name=item['name']).first()
            task.categories.append(category)

        current_app.db.session.add(task)
        current_app.db.session.commit()

        new_data = {
            "name": task.name,
            "description": task.description,
            "duration": task.description,
            "einsenhower_id": eisenhower.type,
            "categories": [{"name": item.name} for item in task.categories]
        }

        return new_data

    # IntegrityError do sqlalchemy é o equivalente ao UniqueViolation do Psycopg2
    except sqlalchemy.exc.IntegrityError:
        return {"msg": "task already exsists!"}, 409


def get_all():

    categories = current_app.db.session.query(CategoriesModel, TasksModel).select_from(
        TasksModel).join(tasks_categories).join(CategoriesModel).all()

    data = [{
        "category": category.CategoriesModel.id,
        "name": category.CategoriesModel.name,
        "description": category.CategoriesModel.description,
        "tasks": [
            {"name": category.TasksModel.name,
                "description": category.TasksModel.description,
                "duration": category.TasksModel.duration,
                "priority": found_eisenhower(category.TasksModel.importance, category.TasksModel.urgency)
             }]} for category in categories]

    return jsonify(data)


def update_one(id):
    try:
        data = request.get_json()
        task = TasksModel.query.filter_by(id=id).first()
        keys = data.keys()
        correct_keys = ['name',
                        'description',
                        'duration',
                        'importance',
                        'urgency',
                        'einsenhower_id']

        if 'importance' in keys:
            if type(data['importance']) != int:
                return {"message": "importance e urgency devem ser inteiros"}, 400
        if 'urgency' in keys:
            if type(data['urgency']) != int:
                return {"message": "importance e urgency devem ser inteiros"}, 400

        for key in keys:
            if key not in correct_keys:
                return {"message": "dado inválido"}, 400

        data['importance'] = data['importance'] if 'importance' in keys else task.importance

        data['urgency'] = data['urgency'] if 'urgency' in keys else task.urgency

        data['categories'] = data['categories'] if 'categories' in keys else task.categories

        keys = list(data)

        eisenhower = EinsenhowersModel.query.filter_by(
            type=found_eisenhower(data['importance'], data['urgency'])).first()
        task.einsenhower_id = eisenhower.id
        current_app.db.session.commit()

        task = TasksModel.query.filter_by(id=id).first()
        for key in keys:
            setattr(task, key, data[key])

        current_app.db.session.commit()

        task = TasksModel.query.get(id)

        new_data = {
            "name": task.name,
            "description": task.description,
            "duration": task.duration,
            "einsenhower_id": eisenhower.type,
            "categories": [{"name": item.name} for item in task.categories]
        }
        return new_data

    # IntegrityError do sqlalchemy é o equivalente ao UniqueViolation do Psycopg2
    except sqlalchemy.exc.IntegrityError:
        return {"msg": "task already exsists!"}, 409


def delete_one(id):
    task = TasksModel.query.filter_by(id=id).first()

    if task == None:
        return {"msg": "task not found!"}, 404

    current_app.db.session.delete(task)
    current_app.db.session.commit()

    return "", 204
