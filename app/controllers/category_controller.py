from flask import jsonify, request, current_app
from app.models.categories_model import CategoriesModel
import sqlalchemy
import pdb


def create_one():
    data = request.get_json()

    try:
        category = CategoriesModel(**data)

        current_app.db.session.add(category)
        current_app.db.session.commit()

        return jsonify(category)
        
    # IntegrityError do sqlalchemy é o equivalente ao UniqueViolation do Psycopg2
    except sqlalchemy.exc.IntegrityError:
        return {"msg": "category already exsists!"}, 409


def get_all():
    categories = CategoriesModel.query.all()
    
    return jsonify(categories)


def update_one(id):
    data = request.get_json()

    category = CategoriesModel.query.filter_by(id=id).first()
    
    if category == None:
        return {"msg": "category not found!"}, 404

    for key in data.keys():
        setattr(category, key, data[key])

    current_app.db.session.add(category)
    current_app.db.session.commit()

    return jsonify(category)


def delete_one(id):
    category = CategoriesModel.query.filter_by(id=id).first()

    if category == None:
        return {"msg": "category not found!"}, 404

    current_app.db.session.delete(category)
    current_app.db.session.commit()

    return "", 204
