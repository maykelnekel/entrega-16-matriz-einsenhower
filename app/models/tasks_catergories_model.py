from dataclasses import dataclass
from app.configs.database import db
from sqlalchemy import Integer, Column, ForeignKey

tasks_categories = db.Table('tasks_categories',

    Column('id', Integer, primary_key=True, nullable=False, unique=True),
    Column('task_id',Integer, ForeignKey('tasks.id'), nullable=True),
    Column('category_id', Integer, ForeignKey('categories.id'), nullable=True)
)
