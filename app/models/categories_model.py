from dataclasses import dataclass
from app.configs.database import db
from app.models.tasks_catergories_model import tasks_categories
from sqlalchemy import String, Integer, Column, Text

@dataclass
class CategoriesModel(db.Model):

    id: int
    name: str
    description: str

    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True, nullable=False, unique=True)
    name = Column(String(100), nullable=False, unique=True)    
    description = Column(Text, nullable=True)

    task = db.relationship("TasksModel", secondary=tasks_categories, backref="categories")