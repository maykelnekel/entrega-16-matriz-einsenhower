from dataclasses import dataclass
from app.configs.database import db
from sqlalchemy import String, Integer, Column


@dataclass
class EinsenhowersModel(db.Model):

    id: int
    type: str

    __tablename__ = 'eisenhowers'

    id = Column(Integer, primary_key=True, nullable=False, unique=True)
    type = Column(String(100), nullable=True)
    
    tasks = db.relationship("TasksModel", backref="eisenhower", uselist=False)
