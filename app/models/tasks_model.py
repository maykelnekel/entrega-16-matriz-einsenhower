from dataclasses import dataclass
from sqlalchemy.orm import backref
from sqlalchemy.sql.schema import ForeignKey
from app.configs.database import db
from app.models.tasks_catergories_model import tasks_categories
from sqlalchemy import String, Integer, Column, ForeignKey

@dataclass
class TasksModel(db.Model):

    id: int
    name: str
    description: str
    duration: int
    importance: int
    urgency: int
    einsenhower_id: int

    __tablename__ = 'tasks'

    id = Column(Integer, primary_key=True, nullable=False, unique=True)
    name = Column(String(100), nullable=False, unique=True)
    description = Column(String, nullable=True)
    duration = Column(Integer, nullable=True)
    importance = Column(Integer, nullable=True)
    urgency = Column(Integer, nullable=True)
    einsenhower_id = Column(Integer, ForeignKey('eisenhowers.id'), nullable=True)
